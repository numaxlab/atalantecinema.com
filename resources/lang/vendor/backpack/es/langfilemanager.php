<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LangFileManager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface for lang files.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'active'                 => 'Activo',
    'cant_edit_online'       => 'Este archivo de idioma no se puede editar online.',
    'code_iso639-1'          => 'Código (ISO 639-1)',
    'default'                => 'Por defecto',
    'empty_file'             => 'No hai traducciones disponibles.',
    'flag_image'             => 'Imagen de la bandera',
    'key'                    => 'Clave',
    'language'               => 'idioma',
    'language_name'          => 'Nombre del idioma',
    'language_text'          => ':language_name texto',
    'language_translation'   => ':language_name tradución',
    'languages'              => 'idiomas',
    'please_fill_all_fields' => 'Por favor, completa todos los campos',
    'rules_text'             => "<strong>Atención: </strong> No traduzcas las palabras prefijadas con dos puntos (ej: ':number_of_items'). Estos serán sustituídos automáticamente con el valor adecuado. Si se traduce, deja de funcionar.",
    'saved'                  => 'Guardado',
    'site_texts'             => 'Textos de la web',
    'switch_to'              => 'Cambiar a',
    'texts'                  => 'Textos',
    'translate'              => 'Traducir',
    'translations'           => 'Traducciones',
    'native_name'            => 'Nombre nativo',

];
