<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack\LangFileManager Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface for lang files.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'active'                 => 'Activo',
    'cant_edit_online'       => 'Este arquivo de idioma non se pode editar online.',
    'code_iso639-1'          => 'Código (ISO 639-1)',
    'default'                => 'Por defecto',
    'empty_file'             => 'Non hai traducións dispoñibles.',
    'flag_image'             => 'Imaxe da bandeira',
    'key'                    => 'Clave',
    'language'               => 'idioma',
    'language_name'          => 'Nome do idioma',
    'language_text'          => ':language_name texto',
    'language_translation'   => ':language_name tradución',
    'languages'              => 'idiomas',
    'please_fill_all_fields' => 'Por favor, completa todos os campos',
    'rules_text'             => "<strong>Atención: </strong> Non traduzas as palabras prefixadas con dous puntos (ex: ':number_of_items'). Estes serán sustituídos automáticamente co valor axeitado. Se se traduce, deixa de funcionar.",
    'saved'                  => 'Gardado',
    'site_texts'             => 'Textos da web',
    'switch_to'              => 'Cambiar a',
    'texts'                  => 'Textos',
    'translate'              => 'Traducir',
    'translations'           => 'Traducións',
    'native_name'            => 'Nome nativo',

];
