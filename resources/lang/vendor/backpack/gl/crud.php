<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new' => 'Gardar e crear novo',
    'save_action_save_and_edit' => 'Gardar e continuar editando',
    'save_action_save_and_back' => 'Gardar e voltar',
    'save_action_changed_notification' => 'A acción por defecto do botón de gardar foi modificada.',

    // Create form
    'add'                 => 'Engadir',
    'back_to_all'         => 'Volver ao listado de',
    'cancel'              => 'Cancelar',
    'add_a_new'           => 'Engadir ',

    // Edit form
    'edit'                 => 'Editar',
    'save'                 => 'Gardar',

    // Revisions
    'revisions'            => 'As revisións',
    'no_revisions'         => 'Non se atoparon revisións',
    'created_this'         => 'creado este',
    'changed_the'          => 'cambiado o',
    'restore_this_value'   => 'restaurar este valor',
    'from'                 => 'desde',
    'to'                   => 'ata',
    'undo'                 => 'Desfacer',
    'revision_restored'    => 'Revisión restaurada correctamente',

    // CRUD table view
    'all'                       => 'Todos os rexistros de ',
    'in_the_database'           => 'na base de datos',
    'list'                      => 'Listar',
    'actions'                   => 'Accións',
    'preview'                   => 'Vista previa',
    'delete'                    => 'Eliminar',
    'admin'                     => 'Admin',
    'details_row'               => 'Esta é a fila de detalles. Modifica ao teu gusto.',
    'details_row_loading_error' => 'Produciuse un erro ao cargar os datos. Por favor, intentao de novo.',

    // Confirmation messages and bubbles
    'delete_confirm'                              => 'Estás seguro de que queres eliminar este elemento?',
    'delete_confirmation_title'                   => 'Elemento eliminado',
    'delete_confirmation_message'                 => 'O elemento foi eliminado de correctamente.',
    'delete_confirmation_not_title'               => 'Non se puido eliminar',
    'delete_confirmation_not_message'             => 'Ocurreu un erro. Pode que o elemento non fora eliminado.',
    'delete_confirmation_not_deleted_title'       => 'Non se puido eliminar',
    'delete_confirmation_not_deleted_message'     => 'Non ocurreu nada. O teu elemento está seguro.',

    // DataTables translation
    'emptyTable'     => 'Non hai datos dispoñibles na táboa',
    'info'           => 'Mostrando rexistros _START_ a _END_ dun total de _TOTAL_ rexistros',
    'infoEmpty'      => 'Mostrando 0 rexistros',
    'infoFiltered'   => '(filtrando de _MAX_ rexistros totais)',
    'infoPostFix'    => '',
    'thousands'      => ',',
    'lengthMenu'     => '_MENU_ elementos por páxina',
    'loadingRecords' => 'Cargando...',
    'processing'     => 'Procesando...',
    'search'         => 'Buscar: ',
    'zeroRecords'    => 'Non se atoparon elementos',
    'paginate'       => [
        'first'    => 'Primeiro',
        'last'     => 'Último',
        'next'     => 'Seguinte',
        'previous' => 'Anterior',
    ],
    'aria' => [
        'sortAscending'  => ': activar para ordear ascendentemente',
        'sortDescending' => ': activar para ordear descendentemente',
    ],

    // global crud - errors
    'unauthorized_access' => 'Acceso denegado - non tes os permisos necesarios para ver esta páxina.',
    'please_fix' => 'Por favor corrixe os seguintes erros:',

    // global crud - success / error notification bubbles
    'insert_success' => 'O elemento foi engadido correctamente.',
    'update_success' => 'O elemento foi modificado correctamente.',

    // CRUD reorder view
    'reorder'                      => 'Reordear',
    'reorder_text'                 => 'Arrastrar e soltar para reordear.',
    'reorder_success_title'        => 'Feito',
    'reorder_success_message'      => 'A orde foi gardada.',
    'reorder_error_title'          => 'Erro',
    'reorder_error_message'        => 'A orde non se gardou.',

    // CRUD yes/no
    'yes' => 'Sí',
    'no' => 'Non',

    // Fields
    'browse_uploads' => 'Subir arqhivos',
    'clear' => 'Limpar',
    'page_link' => 'Ligazón',
    'page_link_placeholder' => 'http://example.com/a-tua-paxina',
    'internal_link' => 'Ligazón interna',
    'internal_link_placeholder' => 'Slug interno. Exemplo: \'admin/page\' (sen comiñas) para \':url\'',
    'external_link' => 'Ligazón externa',

    // elfinder
    'file_manager' => 'Xestor de arquivos',
];
