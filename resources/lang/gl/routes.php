<?php

return [
    'homepage' => '/',
    'film.index' => '/catalogo',
    'film.show' => '/catalogo/{slug}',
    'film.styles' => '/catalogo/{slug}/styles.css',
    'film.host-request.post' => '/catalogo/{slug}/reservar-proxeccion',
    'film-maker.index' => '/cineastas',
    'film-maker.show' => '/cineastas/{slug}',
    'blog-post.index' => '/caderno',
    'blog-post.show' => '/caderno/{slug}',
    'film.submit' => 'envia-o-teu-filme',
    'film.submit.post' => 'envia-o-teu-filme/enviar',
    'about' => 'quen-somos',
    'privacy-policy' => 'politica-de-privacidade',
    'cookie-policy' => 'politica-de-cookies',
    'vod.index' => 'na-casa',
];
