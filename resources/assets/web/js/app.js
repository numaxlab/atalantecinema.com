require('./bootstrap');

/**
 * Common elements
 */
$('.nav-toggle').on('click', function (event) {
    let $this = $(this);
    let targetSelector = $this.data('target');
    let $langToggle = $('.nav-lang-toggle .nav-item');

    if ($langToggle.parent().hasClass('is-active')) {
        $langToggle.trigger('click');
    }

    $this.toggleClass('is-active');

    if ($this.hasClass('is-active')) {
        $(targetSelector).removeClass('is-closed').addClass('is-active');
    } else {
        $(targetSelector).removeClass('is-active').addClass('is-closed');
    }
});

$('.nav-lang-toggle .nav-item').on('click', function (event) {
    let $parent = $(this).parent();
    let targetSelector = '.nav-lang';
    let $navToggle = $('.nav-toggle');

    if ($navToggle.hasClass('is-active')) {
        $navToggle.trigger('click');
    }

    $parent.toggleClass('is-active');

    if ($parent.hasClass('is-active')) {
        $(targetSelector).removeClass('is-closed').addClass('is-active');
    } else {
        $(targetSelector).removeClass('is-active').addClass('is-closed');
    }
});

$('.notification .delete').on('click', function (event) {
    event.preventDefault();

    $(this).parent().remove();
});

$('.cookie-message').cookieBar({
    closeButton: '.cookie-message-close'
});

$('.pagination-more .button-more').on('click', function (event) {
    event.preventDefault();

    let $this = $(this);
    let targetSelector = $this.data('target');
    let endpoint = $this.data('url');

    $.ajax({
        url: endpoint
    }).done(function (data) {
        $(targetSelector).append(data.content);

        if (! data.has_more) {
            $this.addClass('is-hidden');
        } else {
            $this.data('url', data.next);
        }
    });
});
