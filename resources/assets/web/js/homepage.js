require('owl.carousel');

$('.owl-carousel').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    navText: ['&nbsp;', '&nbsp;'],
    navElement: 'div',
    loop: true,
    autoplay: true,
    autoplayTimeout: 6000,
    autoplayHoverPause: true
});
