require('owl.carousel');
const Gallery = require('./gallery');
let imageGallery = null;

$('.nav-opener').on('click', function (event) {
    let $this = $(this);

    $this.toggleClass('is-active');
    $('.nav').toggleClass('is-active');
});

/*function dotdotdotCallback(isTruncated, originalContent) {
    if (! isTruncated) {
        $('a', this).remove();
    }
}

$('.film-synopsis').dotdotdot({
    after: 'a.more',
    watch: 'window',
    callback: dotdotdotCallback
});

$('.film-synopsis').on('click', 'a', function (event) {
    event.preventDefault();

    let $container = $('.film-synopsis');

    if ($(this).hasClass('more')) {
        $container.trigger('destroy').find('a.more').hide();
        $container.addClass('open');
        $('a.less', $container).show();
    } else {
        $(this).hide();
        $container.removeClass('open').dotdotdot({
            after: 'a.more',
            watch: 'window',
            callback: dotdotdotCallback
        });
    }
});*/

$('.owl-carousel').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    navText: ['&nbsp;', '&nbsp;'],
    navElement: 'div',
    loop: true
});

$('.film-block.is-collapsible .film-block-header').on('click', function (event) {
    let $this = $(this);
    let $parent = $this.parent();
    let $blockContent = $parent.children('.film-block-content');

    $parent.children('.film-block-content').toggleClass('is-hidden');

    if ($blockContent.hasClass('is-hidden')) {
        $this.find('.icon')
            .removeClass('icon-angle-up')
            .addClass('icon-angle-down');
    } else {
        $this.find('.icon')
            .removeClass('icon-angle-down')
            .addClass('icon-angle-up');
    }
});

$('.image-modal-opener').on('click', function (event) {
    event.preventDefault();

    $('#image-modal').find('img').attr('src', '');

    let items = [];
    _.forEach($('.images-gallery').children(), function(item) {
        let href = $(item).children('.image-modal-opener').attr('href');
        if (href) {
            items.push(href);
        }
    });
    let liIndex = $(this).parent().index();

    imageGallery = new Gallery(items, liIndex);

    $('#image-modal').find('img').attr('src', imageGallery.current());
    $('#image-modal').addClass('is-active');
});

$('.modal.is-gallery .gallery-prev').on('click', function (event) {
    imageGallery.prev();
    $('#image-modal').find('img').attr('src', imageGallery.current());
});

$('.modal.is-gallery .gallery-next').on('click', function (event) {
    imageGallery.next();
    $('#image-modal').find('img').attr('src', imageGallery.current());
});

$('.modal-close').on('click', function (event) {
    $($(this).data('target')).removeClass('is-active');
});

$('.streaming-media-modal-opener').on('click', function (event) {
    event.preventDefault();

    let target = $(this).data('target');
    $(target).addClass('is-active');
});

$(document).ready(function (event) {
    if (! $('.film-host-request').children('.film-block-content').hasClass('is-hidden')) {
        location.hash = '#film-host-request';
    }

    $(document).keydown(function (event) {
        switch (event.which) {
            case 39: // Arrow right
                console.log('right');
                $('.modal.is-gallery .gallery-next').trigger('click');
                break;
            case 37: // Arrow left
                console.log('left');
                $('.modal.is-gallery .gallery-prev').trigger('click');
                break;
            case 27: // Esc
                console.log('esc');
                $('.modal-close').trigger('click');
                break;
        }
    });
});

$('.vod-modal-opener').on('click', function (event) {
    event.preventDefault();

    let target = $(this).data('target');
    $(target).addClass('is-active');
});

$('.modal-close').on('click', function (event) {
    $($(this).data('target')).removeClass('is-active');
});
