<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $page->getTitle() }}</title>

        @foreach ($page->html() as $meta)
            {!! $meta !!}
        @endforeach

        @foreach ($page->openGraph() as $meta)
            {!! $meta !!}
        @endforeach

        @foreach ($page->twitterCard() as $meta)
            {!! $meta !!}
        @endforeach

        @section('css')
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
            <link href="{{ mix('/assets/web/css/app.css') }}" rel="stylesheet">
        @show

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="apple-mobile-web-app-title" content="Atalante">
        <meta name="application-name" content="Atalante">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "WebSite",
              "name": "{{ config('app.name') }}",
              "url": "{{ Request::url() }}"
            }
        </script>

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

        @yield('html-head')

        @include('web.partials.google-analytics')
    </head>
    <body class="@yield('body-classes')">
        @section('header')
            @include('web.partials.nav.default')
        @show

        @section('main-container')
        <div class="container main-container">
            @include('web.partials.validation-errors')
            @include('web.partials.flash-message')

            @yield('content')
        </div>
        @show

        @section('footer')
            @include('web.partials.footer.default')
        @show

        @include('web.partials.cookie-message')

        @section('js')
        <script src="{{ mix('/assets/web/js/manifest.js') }}"></script>
        <script src="{{ mix('/assets/web/js/vendor.js') }}"></script>
        <script src="{{ mix('/assets/web/js/app.js') }}"></script>
        @show
    </body>
</html>
