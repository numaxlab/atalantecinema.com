@foreach ($films as $film)
    <li class="column is-half-mobile is-one-quarter-tablet">
        @include("web.partials.film.summary-vod", ['film' => $film])
    </li>
@endforeach
