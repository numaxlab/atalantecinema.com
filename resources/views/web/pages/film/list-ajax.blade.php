@foreach ($films as $film)
    <li class="column is-half-tablet is-one-quarter-desktop">
        @include("web.partials.film.summary", ['film' => $film])
    </li>
@endforeach