@extends("web.layouts.master")

@section("content")
<section class="films">
    <h1 class="is-hidden">{{ __('messages.secciones.titulo-peliculas') }}</h1>

    @if (! $films->isEmpty())
        <ul class="films-list columns is-multiline">
        @foreach ($films as $film)
            <li class="column is-half-tablet is-one-quarter-desktop">
                @include("web.partials.film.summary", ['film' => $film])
            </li>
        @endforeach
        </ul>

        @if ($films->hasMorePages())
        <div class="pagination-more columns">
            <div class="column is-6 is-offset-3">
                <a href="" class="button button-more" data-target=".films-list" data-url="{{ $films->nextPageUrl() }}">{{ __('messages.acciones.ver-mas') }}</a>
            </div>
        </div>
        @endif
    @endif
</section>
@endsection