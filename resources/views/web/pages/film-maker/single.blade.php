@extends('web.layouts.master')

@section('content')
<article class="film-maker" itemscope itemtype="http://schema.org/Person">
    <div class="columns">
        <div class="column is-7">
            <h1 class="title is-2" itemprop="name">{{ $filmMaker->fullname }}</h1>

            @if (! empty($filmMaker->biography))
                <div class="content" itemprop="description">
                    {!! $filmMaker->biography !!}
                </div>
            @endif
        </div>
        <div class="column is-4 is-offset-1">
            <figure itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                <img src="{{ ViewHelper::imgSrc($filmMaker, 'photo_file_path', 'resizeCrop,768,1104') }}" alt="" itemprop="contentUrl" />
                @if (! empty($filmMaker->photo_caption))
                <figcaption>{{ $filmMaker->photo_caption }}</figcaption>
                @endif
            </figure>
        </div>
    </div>

    @if (! $filmMaker->films->isEmpty())
    <section class="film-maker-films">
        <h2 class="title is-4">{{ __('messages.cineastas-titulo-peliculas') }}</h2>
        <ul>
        @foreach ($filmMaker->films as $film)
            <li>
                @include('web.partials.film.summary-fullwidth', ['film' => $film])
            </li>
        @endforeach
        </ul>
    </section>
    @endif
</article>
@endsection
