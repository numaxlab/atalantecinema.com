@extends("web.layouts.master")

@section("content")
<section class="film-makers">
    <h1 class="is-hidden">{{ __('messages.secciones.titulo-cineastas') }}</h1>

    @if (! $filmMakers->isEmpty())
        <ul class="film-makers-list columns is-multiline">
            @foreach ($filmMakers as $filmMaker)
                <li class="column is-one-third">
                    @include("web.partials.film-maker.summary", ['blogPost' => $filmMaker])
                </li>
            @endforeach
        </ul>

        @if ($filmMakers->hasMorePages())
        <div class="pagination-more columns">
            <div class="column is-6 is-offset-3">
                <a href="" class="button button-more" data-target=".film-makers-list" data-url="{{ $filmMakers->nextPageUrl() }}">{{ __('messages.acciones.ver-mas') }}</a>
            </div>
        </div>
        @endif
    @endif
</section>
@endsection