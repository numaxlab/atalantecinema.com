@extends('web.layouts.master')

@section('content')
    <article class="page">
        <div class="wrapper">
            <h1 class="title is-2">{{ $page->name }}</h1>
            <div class="content">
                {!! $page->content !!}
            </div>
        </div>
    </article>
@endsection