@extends('web.layouts.master')

@section('content')
<article class="blog-post" itemscope itemtype="http://schema.org/BlogPosting">
    <div class="wrapper">
        <h1 class="title is-2" itemprop="name">{{ $blogPost->title }}</h1>

        <div class="intro content">
            {!! $blogPost->intro !!}
        </div>

        @if (! empty($blogPost->inner_image_file_path))
        <div class="image">
            @if (! empty($blogPost->inner_image_link))
            <a href="{{ $blogPost->inner_image_link }}" target="_blank">
                <img src="{{ ViewHelper::imgSrc($blogPost, 'inner_image_file_path', 'resizeCrop,960,596') }}" alt="" class="blog-post-image" />
            </a>
            @else
            <img src="{{ ViewHelper::imgSrc($blogPost, 'inner_image_file_path', 'resizeCrop,960,596') }}" alt="" class="blog-post-image" />
            @endif
        </div>
        @endif

        @if (! empty($blogPost->original_author))
            <p class="author-info">{{ $blogPost->original_author }}</p>
        @endif

        <time datetime="{{ $blogPost->published_at->toAtomString() }}" class="published-at" itemprop="datePublished">{{ $blogPost->published_at->format('d/m/Y') }}</time>

        @if (!empty($blogPost->body))
        <div class="content" itemprop="articleBody">
            {!! $blogPost->body !!}
        </div>
        @endif
    </div>

    @if (! $blogPost->films->isEmpty())
    <section class="blog-post-films">
        <ul>
        @foreach ($blogPost->films as $film)
            <li>
                @include('web.partials.film.summary-fullwidth', ['film' => $film])
            </li>
        @endforeach
        </ul>
    </section>
    @endif

    @if ($relatedBlogPosts->isNotEmpty())
    <section class="blog-post-related">
        <h2 class="title is-3">{{ __('messages.cuaderno-titulo-relacionadas') }}</h2>
        <ul class="columns">
        @foreach ($relatedBlogPosts as $blogPost)
            <li class="column is-one-third">
                @include('web.partials.blog-post.summary', ['blogPost' => $blogPost])
            </li>
        @endforeach
        </ul>
    </section>
    @endif
</article>
@endsection
