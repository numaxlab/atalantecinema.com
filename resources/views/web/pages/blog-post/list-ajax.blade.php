@foreach ($blogPosts as $blogPost)
    <li class="column is-one-third" itemprop="blogPost">
        @include("web.partials.blog-post.summary", ['blogPost' => $blogPost, 'hasImage' => true])
    </li>
@endforeach