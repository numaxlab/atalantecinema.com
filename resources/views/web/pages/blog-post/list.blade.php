@extends("web.layouts.master")

@section("content")
<section class="blog" itemscope itemtype="http://schema.org/Blog">
    <h1 class="is-hidden">{{ __('messages.secciones.titulo-cuaderno') }}</h1>

    @if (! $blogPosts->isEmpty())
        <ul class="blog-posts-list columns is-multiline">
            @foreach ($blogPosts as $blogPost)
                <li class="column is-one-third" itemprop="blogPost">
                    @include("web.partials.blog-post.summary", ['blogPost' => $blogPost, 'hasImage' => true])
                </li>
            @endforeach
        </ul>

        @if ($blogPosts->hasMorePages())
        <div class="pagination-more columns">
            <div class="column is-6 is-offset-3">
                <a href="" class="button button-more" data-target=".blog-posts-list" data-url="{{ $blogPosts->nextPageUrl() }}">{{ __('messages.acciones.ver-mas') }}</a>
            </div>
        </div>
        @endif
    @endif
</section>
@endsection