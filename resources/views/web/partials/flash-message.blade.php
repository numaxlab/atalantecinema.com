@if (session('flash-message'))
    <div class="notification is-success">
        <button class="delete"></button>
        {{ session('flash-message') }}
    </div>
@endif