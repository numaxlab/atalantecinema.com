<article class="film-summary" itemscope itemtype="http://schema.org/Movie">
    <a href="{{ route('film.show', $film->slug) }}" class="film-link" itemprop="url">
        <div class="image">
            <img src="{{ ViewHelper::imgSrc($film, 'poster_file_path', 'resizeCrop,768,1099') }}" alt="" />
        </div>
        <h3 class="title is-4">{{ $film->title }}</h3>
        <p class="subtitle is-5">{{ ViewHelper::showFilmFilmMakers($film->makers) }}</p>
    </a>
</article>