<article class="film-summary-fullwidth" itemscope itemtype="http://schema.org/Movie">
    <a href="{{ route('film.show', $film->slug) }}" class="film-link" itemprop="url">
        <img src="{{ ViewHelper::imgSrc($film, 'header_image_file_path', 'resizeCrop,1344,432') }}" alt="" />
        <h3 class="title is-3">{{ $film->title }}</h3>
    </a>
</article>