<article class="film-summary is-vod">
    <div class="film-hover-wrapper" onclick="void(0)">
        <div class="image-actions-wrapper">
            <div class="image">
                <img src="{{ ViewHelper::imgSrc($film, 'poster_file_path', 'resizeCrop,768,1099') }}" alt="" />
            </div>

            <ul class="actions">
                @if ($film->vod_film_link && (empty($hideTitle) || $hideTitle === false))
                    <li class="action">
                        <a href="{{ $film->vod_film_link }}">
                            {{ __('messages.acciones.ver-ficha') }}
                        </a>
                    </li>
                @endif
                @if ($film->vod_price)
                    <li class="action">
                        <a href="#" class="vod-modal-opener" data-target="#vod-modal-film-{{ $film->id }}">
                            {{ __('messages.acciones.alquilar') }} ({{ ViewHelper::moneyFormat($film->vod_price) }})
                        </a>
                    </li>
                @endif
                @if ($film->vod_buy_price)
                    <li class="action">
                        <a href="{{ $film->vod_link }}" target="_blank">
                            {{ __('messages.acciones.comprar') }} ({{ ViewHelper::moneyFormat($film->vod_buy_price) }})
                        </a>
                    </li>
                @endif
            </ul>
        </div>

        <div class="price">
            <p>{{ __('messages.secciones.titulo-vod') }}</p>
        </div>
    </div>

    @if (empty($hideTitle) || $hideTitle === false)
        <h3 class="title is-4">{{ $film->title }}</h3>
        <p class="subtitle is-5">{{ ViewHelper::showFilmFilmMakers($film->makers) }}</p>
    @endif

    @if ($film->vod_embed)
        <div class="modal" id="vod-modal-film-{{ $film->id }}">
            <div class="modal-background"></div>
            <div class="modal-content">
                <div class="video-container">
                    {!! $film->vod_embed !!}
                </div>
            </div>
            <button class="modal-close" data-target="#vod-modal-film-{{ $film->id }}"></button>
        </div>
    @endif
</article>