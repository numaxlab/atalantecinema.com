<article class="blog-post-summary" itemscope itemtype="http://schema.org/BlogPosting">
    <a href="{{ route('blog-post.show', $blogPost->slug) }}" class="blog-post-link" itemprop="url">
        @if (! empty($hasImage))
            <div class="image is-1by1">
                <img src="{{ ViewHelper::imgSrc($blogPost, 'image_file_path', 'resizeCrop,768,768') }}" alt="" />
            @if (! $blogPost->films->isEmpty())
                <p class="related-film">{{ implode(', ', $blogPost->films->pluck('title')->toArray()) }}</p>
            @endif
            </div>
        @endif
        <h3 class="title is-4">{{ $blogPost->title }}</h3>
    </a>

    <div class="intro">
        {!! $blogPost->intro !!}
    </div>
</article>