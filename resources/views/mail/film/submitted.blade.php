@component('mail::message')
# {{ __('messages.envia-pelicula-titulo-notification') }}

    @foreach($formData as $key => $value)
        - {{ $key }}: {{ empty($value) ? '--' : $value }}
    @endforeach
@endcomponent
