@extends('web.layouts.master')

@section('content')
  <div class="error-page">
    <h1 class="title is-1">500</h1>
    <p class="subtitle">{{ __('messages.500') }}</p>
    <div>
        <?php $defaultErrorMessage = '<a href="'.route('homepage').'">'.__('messages.volver-portada').'</a>'; ?>
      @if (config('app.debug'))
        {!! isset($exception)? ($exception->getMessage() ? $exception->getMessage() : $defaultErrorMessage) : $defaultErrorMessage !!}
      @else
        <a href="{{ route('homepage') }}">{!! $defaultErrorMessage !!}</a>
      @endif
    </div>
  </div>
@endsection