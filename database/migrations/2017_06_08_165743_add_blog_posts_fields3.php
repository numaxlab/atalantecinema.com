<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBlogPostsFields3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_posts', function (Blueprint $table) {
            $table->string('inner_image_file_path')->nullable();
            $table->string('inner_image_link')->nullable();
        });

        Schema::table('blog_post_translations', function (Blueprint $table) {
            $table->text('body')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_post_translations', function (Blueprint $table) {
            $table->text('body')->change();
        });

        Schema::table('blog_posts', function (Blueprint $table) {
            $table->dropColumn('inner_image_link');
            $table->dropColumn('inner_image_file_path');
        });
    }
}
