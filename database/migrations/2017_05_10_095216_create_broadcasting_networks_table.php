<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastingNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broadcasting_networks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('url')->nullable();

            $table->timestamps();
        });

        Schema::create('broadcasting_network_film', function (Blueprint $table) {
            $table->unsignedInteger('broadcasting_network_id');
            $table->unsignedInteger('film_id');

            $table->foreign('broadcasting_network_id')
                ->references('id')
                ->on('broadcasting_networks');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broadcasting_network_film');
        Schema::dropIfExists('broadcasting_networks');
    }
}
