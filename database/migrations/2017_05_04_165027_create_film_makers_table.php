<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmMakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_makers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('surname');
            $table->string('slug');
            $table->string('photo_file_path');

            $table->timestamps();
        });

        Schema::create('film_maker_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_maker_id');
            $table->string('locale')->index();

            $table->text('biography')->nullable();

            $table->unique(['film_maker_id', 'locale']);
            $table->foreign('film_maker_id')
                ->references('id')
                ->on('film_makers')
                ->onDelete('cascade');
        });

        Schema::table('films', function (Blueprint $table) {
            $table->unsignedInteger('film_maker_id');

            $table->foreign('film_maker_id')
                ->references('id')
                ->on('film_makers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->dropForeign('films_film_maker_id_foreign');
            $table->dropColumn('film_maker_id');
        });

        Schema::dropIfExists('film_maker_translations');
        Schema::dropIfExists('film_makers');
    }
}
