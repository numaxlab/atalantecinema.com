<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReorderFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film_makers', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });

        Schema::table('film_images', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });

        Schema::table('film_awards', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });

        Schema::table('film_quotes', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });

        Schema::table('film_resources', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });

        Schema::table('film_streaming_media', function (Blueprint $table) {
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film_streaming_media', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');
        });

        Schema::table('film_resources', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');
        });

        Schema::table('film_quotes', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');
        });

        Schema::table('film_awards', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');
        });

        Schema::table('film_images', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');
        });

        Schema::table('film_makers', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');
        });
    }
}
