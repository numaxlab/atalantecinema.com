<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFilmsReleaseDateReorder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->dropColumn('release_date');
            $table->dropColumn('classification');

            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
        });

        Schema::table('film_translations', function (Blueprint $table) {
            $table->string('release_date');
            $table->string('classification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film_translations', function (Blueprint $table) {
            $table->dropColumn('classification');
            $table->dropColumn('release_date');
        });

        Schema::table('films', function (Blueprint $table) {
            $table->dropColumn('rgt');
            $table->dropColumn('lft');
            $table->dropColumn('depth');
            $table->dropColumn('parent_id');

            $table->string('classification');
            $table->date('release_date')->nullable();
        });
    }
}
