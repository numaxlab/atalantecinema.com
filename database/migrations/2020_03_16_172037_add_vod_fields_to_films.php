<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVodFieldsToFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->boolean('has_vod')->default(0);
            $table->text('vod_embed')->nullable();
            $table->string('vod_link')->nullable();
            $table->float('vod_price')->nullable();
            $table->integer('vod_sort')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->dropColumn('has_vod');
            $table->dropColumn('vod_embed');
            $table->dropColumn('vod_link');
            $table->dropColumn('vod_price');
            $table->dropColumn('vod_sort');
        });
    }
}
