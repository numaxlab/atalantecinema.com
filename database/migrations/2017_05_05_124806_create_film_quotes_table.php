<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_quotes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('media');
            $table->string('url')->nullable();

            $table->unsignedInteger('film_id');

            $table->foreign('film_id')
                ->references('id')
                ->on('films')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('film_quote_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('film_quote_id');
            $table->string('locale')->index();

            $table->text('quote');

            $table->unique(['film_quote_id', 'locale']);
            $table->foreign('film_quote_id')
                ->references('id')
                ->on('film_quotes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_quote_translations');
        Schema::dropIfExists('film_quotes');
    }
}
