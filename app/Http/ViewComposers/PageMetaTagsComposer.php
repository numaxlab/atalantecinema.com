<?php


namespace App\Http\ViewComposers;

use App\Repositories\PageMetaTagsBag;
use Illuminate\View\View;
use SocialLinks\Page;

class PageMetaTagsComposer
{
    /**
     * @var Page
     */
    protected $page;

    /**
     * PageMetaTagsComposer constructor.
     * @param \App\Repositories\PageMetaTagsBag $pageMetaTagsBag
     */
    public function __construct(PageMetaTagsBag $pageMetaTagsBag)
    {
        $this->page = $pageMetaTagsBag->getSocialLinksPage();
    }

    /**
     * @param \Illuminate\View\View $view
     */
    public function compose(View $view)
    {
        $view->with('page', $this->page);
    }
}
