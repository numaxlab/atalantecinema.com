<?php

namespace App\Http\Controllers;

use App\Models\FilmMaker;
use App\Repositories\Criteria\SortBy;
use App\Repositories\PageMetaTagsBag;
use ModelManager;
use Illuminate\Http\Request;
use ViewHelper;

class FilmMakerController extends Controller
{
    private $filmMakerRepository;

    public function __construct(PageMetaTagsBag $pageMetaTagsBag) {
        parent::__construct($pageMetaTagsBag);

        $this->filmMakerRepository = ModelManager::getRepository(FilmMaker::class);
    }

    public function index(Request $request)
    {
        $filmMakers = $this->filmMakerRepository
            ->pushCriteria(new SortBy('lft'))
            ->paginate(12);

        if ($request->ajax()) {
            $content = view('web.pages.film-maker.list-ajax', compact('filmMakers'))
                ->render();

            return [
                'has_more' => $filmMakers->hasMorePages(),
                'next' => $filmMakers->nextPageUrl(),
                'content' => $content
            ];
        }

        $this->pageMetaTagsBag
            ->setTitle(__('messages.secciones.titulo-cineastas'));

        return view('web.pages.film-maker.list', compact('filmMakers'));
    }

    public function show($slug)
    {
        /** @var FilmMaker $filmMaker */
        $filmMaker = $this->filmMakerRepository->findBy('slug', $slug, ['films']);

        $this->pageMetaTagsBag
            ->setTitle($filmMaker->fullname)
            ->setText($filmMaker->biography)
            ->setImage((string) ViewHelper::imgSrc($filmMaker, 'photo_file_path', 'resizeCrop,637,333'));

        return view('web.pages.film-maker.single', compact('filmMaker'));
    }
}
