<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PageCrudRequest as StoreRequest;
use App\Http\Requests\Admin\PageCrudRequest as UpdateRequest;
use App\Models\Page;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PageCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Page::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pages');
        $this->crud->setEntityNameStrings('páxina', 'páxinas');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'label' => 'Nome'
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome ES',
                'name' => 'name:es'
            ],
            [
                'label' => 'Nome GL',
                'name' => 'name:gl'
            ],
            [
                'label' => 'Slug ES',
                'name' => 'slug:es'
            ],
            [
                'label' => 'Slug GL',
                'name' => 'slug:gl'
            ],
            [
                'label' => 'Contido ES',
                'name' => 'content:es',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Contido GL',
                'name' => 'content:gl',
                'type' => 'wysiwyg'
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
