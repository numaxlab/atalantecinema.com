<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilmQuoteCrudRequest;
use App\Models\Film;
use App\Models\FilmQuote;

class FilmQuoteCrudController extends FilmNestedCrudController
{
    public function setup()
    {
        $filmId = $this->getFilmId();

        $this->crud->setModel(FilmQuote::class);
        $this->setRouteParameterName('quote');
        $this->setRouteSuffix('quotes');
        $this->setEntityNameString('cita', 'citas');
        $this->addFilmClause();

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'label' => 'Filme',
                'type' => 'select',
                'name' => 'film_id',
                'entity' => 'film',
                'attribute' => 'title',
                'model' => Film::class
            ],
            [
                'name' => 'quote',
                'label' => 'Cita',
            ],
            [
                'name' => 'media',
                'label' => 'Medio'
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Cita ES',
                'name' => 'quote:es'
            ],
            [
                'label' => 'Cita GL',
                'name' => 'quote:gl'
            ],
            [
                'label' => 'Medio',
                'name' => 'media'
            ],
            [
                'label' => 'Ligazón',
                'name' => 'url',
                'type' => 'url'
            ],
            [
                'name' => 'film_id',
                'type' => 'hidden',
                'value' => $filmId
            ]
        ]);

        $this->crud->enableReorder('quote', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmQuoteCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmQuoteCrudRequest $request)
    {
        return parent::updateCrud();
    }
}
