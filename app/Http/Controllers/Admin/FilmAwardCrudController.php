<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilmAwardStoreRequest;
use App\Http\Requests\Admin\FilmAwardUpdateRequest;
use App\Models\Film;
use App\Models\FilmAward;

class FilmAwardCrudController extends FilmNestedCrudController
{
    public function setup()
    {
        $filmId = $this->getFilmId();

        $this->crud->setModel(FilmAward::class);
        $this->setRouteParameterName('award');
        $this->setRouteSuffix('awards');
        $this->setEntityNameString('premio', 'premios');
        $this->addFilmClause();

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'label' => 'Filme',
                'type' => 'select',
                'name' => 'film_id',
                'entity' => 'film',
                'attribute' => 'title',
                'model' => Film::class
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
            [
                'name' => 'logo_file_path',
                'label' => 'Logo',
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome',
                'name' => 'name'
            ],
            [
                'label' => 'Logo',
                'name' => 'logo_file_path',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public'
            ],
            [
                'name' => 'film_id',
                'type' => 'hidden',
                'value' => $filmId
            ]
        ]);

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmAwardStoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmAwardUpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
