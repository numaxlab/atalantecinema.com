<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilmImageStoreRequest;
use App\Http\Requests\Admin\FilmImageUpdateRequest;
use App\Models\Film;
use App\Models\FilmImage;

class FilmImageCrudController extends FilmNestedCrudController
{
    public function setup()
    {
        $filmId = $this->getFilmId();

        $this->crud->setModel(FilmImage::class);
        $this->setRouteParameterName('image');
        $this->setRouteSuffix('images');
        $this->setEntityNameString('imaxe', 'imaxes');
        $this->addFilmClause();

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'label' => 'Filme',
                'type' => 'select',
                'name' => 'film_id',
                'entity' => 'film',
                'attribute' => 'title',
                'model' => Film::class
            ],
            [
                'label' => 'Imaxe',
                'type' => 'model_function',
                'function_name' => 'getThumbnailImgTag'
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Imaxe',
                'name' => 'file_path',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'public',
                'hint' => 'Tamaño mínimo: 768x432',
            ],
            [
                'label' => 'Nome ES',
                'name' => 'name:es'
            ],
            [
                'label' => 'Nome GL',
                'name' => 'name:gl'
            ],
            [
                'name' => 'film_id',
                'type' => 'hidden',
                'value' => $filmId
            ]
        ]);

        $this->crud->enableReorder('thumbnail', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmImageStoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmImageUpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
