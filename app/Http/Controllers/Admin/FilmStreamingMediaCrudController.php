<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FilmStreamingMediaCrudRequest;
use App\Models\Film;
use App\Models\FilmStreamingMedia;

class FilmStreamingMediaCrudController extends FilmNestedCrudController
{
    public function setup()
    {
        $filmId = $this->getFilmId();

        $this->crud->setModel(FilmStreamingMedia::class);
        $this->setRouteParameterName('streaming_sedium');
        $this->setRouteSuffix('streaming-media');
        $this->setEntityNameString('medio de visualización', 'medios de visualización');
        $this->addFilmClause();

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ],
            [
                'name' => 'starred',
                'label' => 'Destacado',
                'type' => 'boolean'
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome ES',
                'name' => 'name:es'
            ],
            [
                'label' => 'Nome GL',
                'name' => 'name:gl'
            ],
            [
                'label' => 'URL',
                'name' => 'url',
                'type' => 'url'
            ],
            [
                'label' => 'Embed',
                'name' => 'embed',
                'type' => 'textarea'
            ],
            [
                'label' => 'Destacado',
                'name' => 'starred',
                'type' => 'checkbox'
            ],
            [
                'name' => 'film_id',
                'type' => 'hidden',
                'value' => $filmId
            ]
        ]);

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');
    }

    public function store(FilmStreamingMediaCrudRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(FilmStreamingMediaCrudRequest $request)
    {
        return parent::updateCrud();
    }
}
