<?php

namespace App\Http\Controllers\Admin;

use App\Models\Film;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Route;

abstract class FilmNestedCrudController extends CrudController
{
    /**
     * @var Film
     */
    protected $film;

    /**
     * @var string
     */
    protected $routeParameterName;

    public function setRouteParameterName($name)
    {
        $this->routeParameterName = $name;
    }

    protected function getFilmId()
    {
        return Route::current()->parameter('filmId');
    }

    protected function getFilm()
    {
        if (empty($this->film)) {
            $this->film = Film::find($this->getFilmId());
        }

        return $this->film;
    }

    protected function setRouteSuffix($suffix)
    {
        $film = $this->getFilm();

        $this->crud->setRoute(config('backpack.base.route_prefix').'/films/'.$film->id.'/'.$suffix);
    }

    protected function setEntityNameString($singular, $plural)
    {
        $film = $this->getFilm();

        $this->crud->setEntityNameStrings($singular.' do filme '.$film->title, $plural.' do filme '.$film->title);
    }

    public function addFilmClause()
    {
        $film = $this->getFilm();

        $this->crud->addClause('where', 'film_id', '=', $film->id);
    }

    public function edit($id)
    {
        $id = Route::current()->parameter($this->routeParameterName);

        return parent::edit($id);
    }

    public function destroy($id)
    {
        $id = Route::current()->parameter($this->routeParameterName);

        return parent::destroy($id);
    }
}
