<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BroadcastingNetworkCrudRequest as StoreRequest;
use App\Http\Requests\Admin\BroadcastingNetworkCrudRequest as UpdateRequest;
use App\Models\BroadcastingNetwork;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class BroadcastingNetworkCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(BroadcastingNetwork::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/broadcasting-networks');
        $this->crud->setEntityNameStrings('rede de difusión', 'redes de difusión');

        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'label' => 'Nome',
            ]
        ]);

        $this->crud->addFields([
            [
                'label' => 'Nome',
                'name' => 'name'
            ],
            [
                'label' => 'Web',
                'name' => 'url',
                'type' => 'url'
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
