<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use App\Repositories\Criteria\ByTranslatedSlug;
use App\Repositories\Criteria\IsPublishedNow;
use App\Repositories\Criteria\SortBy;
use App\Repositories\PageMetaTagsBag;
use ModelManager;
use Illuminate\Http\Request;
use ViewHelper;

class BlogPostController extends Controller
{
    /**
     * @var \App\Repositories\BlogPostRepository
     */
    private $blogPostRepository;

    public function __construct(PageMetaTagsBag $pageMetaTagsBag) {
        parent::__construct($pageMetaTagsBag);

        $this->blogPostRepository = ModelManager::getRepository(BlogPost::class);
    }

    public function index(Request $request)
    {
        $blogPosts = $this->blogPostRepository
            ->pushCriteria(new IsPublishedNow())
            ->pushCriteria(new SortBy('published_at', 'DESC'))
            ->paginate(6, ['films']);

        if ($request->ajax()) {
            $content = view('web.pages.blog-post.list-ajax', compact('blogPosts'))
                ->render();

            return [
                'has_more' => $blogPosts->hasMorePages(),
                'next' => $blogPosts->nextPageUrl(),
                'content' => $content
            ];
        }

        $this->pageMetaTagsBag
            ->setTitle(__('messages.secciones.titulo-cuaderno'));

        return view('web.pages.blog-post.list', compact('blogPosts'));
    }

    public function show($slug)
    {
        /** @var BlogPost $blogPost */
        $blogPost = $this->blogPostRepository
            ->pushCriteria(new ByTranslatedSlug($slug))
            ->pushCriteria(new IsPublishedNow())
            ->firstOrFail(['films']);

        $relatedBlogPosts = $this->blogPostRepository
            ->findRelated($blogPost);

        $this->pageMetaTagsBag
            ->setTitle($blogPost->title)
            ->setText($blogPost->intro)
            ->setImage((string) ViewHelper::imgSrc($blogPost, 'image_file_path', 'resizeCrop,637,333'));

        return view('web.pages.blog-post.single', compact('blogPost', 'relatedBlogPosts'));
    }
}
