<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class BlogPostStoreRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'title:es' => 'required',
            'title:gl' => 'required',
            'slug:es' => 'nullable|alpha_dash',
            'slug:gl' => 'nullable|alpha_dash',
            'intro:es' => 'required',
            'intro:gl' => 'required',
            'body:es' => 'nullable',
            'body:gl' => 'nullable',
            'image_file_path' => 'required|mimes:png,jpeg|dimensions:min_width=960,min_height=596',
            'inner_image_file_path' => 'nullable|mimes:png,jpeg|dimensions:min_width=960,min_height=596',
            'published_at' => 'required|date'
        ];
    }
}
