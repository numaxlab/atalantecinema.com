<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class FilmStreamingMediaCrudRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'name:es' => 'required',
            'name:gl' => 'required',
            'url' => 'nullable|url',
            'embed' => '',
            'starred' => 'boolean'
        ];
    }
}
