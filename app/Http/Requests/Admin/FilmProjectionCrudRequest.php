<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class FilmProjectionCrudRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'cinema_id' => 'required',
            'starts_at' => 'required|date',
            'ends_at' => 'nullable|date',
            'info:es' => '',
            'info:gl' => '',
            'ticket_purchase_url' => 'nullable|url',
            'published' => 'boolean'
        ];
    }
}
