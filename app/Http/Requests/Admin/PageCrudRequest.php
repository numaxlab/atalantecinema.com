<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class PageCrudRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'name:es' => 'required',
            'name:gl' => 'required',
            'slug:es' => 'nullable|alpha_dash',
            'slug:gl' => 'nullable|alpha_dash',
            'content:es' => 'required',
            'content:gl' => 'required',
        ];
    }
}
