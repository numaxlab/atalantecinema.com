<?php

namespace App\Http\Requests\Admin;

use Auth;
use Backpack\CRUD\app\Http\Requests\CrudRequest;

class FilmQuoteCrudRequest extends CrudRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'quote:es' => 'required',
            'quote:gl' => 'required',
            'media' => 'required',
            'url' => 'nullable|url'
        ];
    }
}
