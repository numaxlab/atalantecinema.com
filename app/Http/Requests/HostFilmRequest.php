<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HostFilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_person' => 'required',
            'cinema' => 'required',
            'format' => '',
            'phone_number' => '',
            'email' => 'required|email',
            'comments' => '',
            'privacy_policy' => 'required'
        ];
    }
}
