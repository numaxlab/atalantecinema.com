<?php


namespace App\Repositories\Criteria;


use Ablunier\Laravel\Database\Contracts\Repository\Criteria;
use Ablunier\Laravel\Database\Contracts\Repository\Repository;

class IsPublished implements Criteria
{

    /**
     * @param $model
     * @param Repository $repository
     *
     * @return mixed
     */
    public function apply($model, Repository $repository) {
        return $model->where('published', true);
    }
}
