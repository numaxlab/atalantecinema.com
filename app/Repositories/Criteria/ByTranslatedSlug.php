<?php


namespace App\Repositories\Criteria;


use Ablunier\Laravel\Database\Contracts\Repository\Criteria;
use Ablunier\Laravel\Database\Contracts\Repository\Repository;

class ByTranslatedSlug implements Criteria
{
    /**
     * @var string
     */
    private $slug;

    /**
     * ByTranslatedSlug constructor.
     * @param string $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }

    public function apply($model, Repository $repository)
    {
        return $model->whereTranslation('slug', $this->slug);
    }
}
