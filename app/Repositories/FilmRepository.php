<?php


namespace App\Repositories;


use Ablunier\Laravel\Database\Repository\Eloquent\Repository;
use App\Models\Film;
use App\Repositories\Criteria\Without;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class FilmRepository
 * @package App\Repositories
 * @property \App\Models\Film $model
 */
class FilmRepository extends Repository
{
    /**
     * @param array $with
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findAllInHomepage(array $with = [])
    {
        $this->addWithCriteria($with);
        $this->applyCriteria();

        $result = $this->model
            ->where('published', true)
            ->where('is_homepage', true)
            ->orderBy('homepage_sort')
            ->get();

        $this->refresh();

        return $result;
    }

    /**
     * @param array $with
     * @return \Illuminate\Contracts\Pagination\Paginator|null
     */
    public function paginateAllPublished(array $with = [])
    {
        $this->addWithCriteria($with);
        $this->applyCriteria();

        $result = $this->model
            ->where('published', true)
            ->orderBy('lft')
            ->simplePaginate(12);

        $this->refresh();

        return $result;
    }

    /**
     * @param array $with
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function findAllPublished(array $with = [])
    {
        $this->addWithCriteria($with);
        $this->applyCriteria();

        $result = $this->model
            ->where('published', true)
            ->orderBy('lft')
            ->get();

        $this->refresh();

        return $result;
    }

    /**
     * @param array $with
     * @return Collection
     */
    public function paginateVod(array $with = [])
    {
        $this->addWithCriteria($with);
        $this->applyCriteria();

        $result = $this->model
            ->where('published', true)
            ->where('has_vod', true)
            ->orderBy('vod_sort')
            ->orderBy('lft')
            ->simplePaginate(16);

        $this->refresh();

        return $result;
    }
}
