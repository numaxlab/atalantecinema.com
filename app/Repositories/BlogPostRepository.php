<?php

namespace App\Repositories;

use Ablunier\Laravel\Database\Repository\Eloquent\Repository;
use App\Models\BlogPost;
use App\Repositories\Criteria\IsPublishedNow;

/**
 * Class BlogPostRepository
 * @package App\Repositories
 * @property \App\Models\BlogPost $model
 */
class BlogPostRepository extends Repository
{
    const HOMEPAGE_POSTS_QTY = 3;

    /**
     * @param array $with
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAllInHomepage(array $with = [])
    {
        $this->pushCriteria(new IsPublishedNow());
        $this->addWithCriteria($with);
        $this->applyCriteria();

        $isHomepage = $this->model
            ->where('is_homepage', true)
            ->orderByDesc('published_at')
            ->limit(self::HOMEPAGE_POSTS_QTY)
            ->get();

        $this->refresh();

        if ($isHomepage->count() < self::HOMEPAGE_POSTS_QTY) {
            $latestQty = self::HOMEPAGE_POSTS_QTY - $isHomepage->count();

            $this->pushCriteria(new IsPublishedNow());
            $this->addWithCriteria($with);
            $this->applyCriteria();

            $latest = $this->model
                ->where('is_homepage', false)
                ->orderByDesc('published_at')
                ->limit($latestQty)
                ->get();

            $this->refresh();

            $result = $isHomepage->merge($latest);
        } else {
            $result = $isHomepage;
        }

        return $result;
    }

    /**
     * @param \App\Models\BlogPost $blogPost
     * @param int $qty
     * @param array $with
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findRelated(BlogPost $blogPost, $qty = 3, array $with = [])
    {
        $this->addWithCriteria($with);
        $this->applyCriteria();

        $result = $this->model
            ->where('id', '!=', $blogPost->id)
            ->inRandomOrder()
            ->limit($qty)
            ->get();

        $this->refresh();

        return $result;
    }
}
