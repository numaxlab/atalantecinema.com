<?php

namespace App\Repositories;

use SocialLinks\Page;
use Request;

class PageMetaTagsBag
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var string
     */
    protected $twitterUser;

    public function __construct()
    {
        $this->url = Request::url();
    }

    /**
     * @param string $url
     * @return PageMetaTagsBag
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string $title
     * @return PageMetaTagsBag
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $text
     * @return PageMetaTagsBag
     */
    public function setText($text) {
        $this->text = $text;
        return $this;
    }

    /**
     * @param string $image
     * @return PageMetaTagsBag
     */
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }

    /**
     * @param string $twitterUser
     * @return PageMetaTagsBag
     */
    public function setTwitterUser($twitterUser) {
        $this->twitterUser = $twitterUser;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle() {
        if (empty($this->title)) {
            $this->title = trans('messages.secciones.titulo-general');
        } else {
            $this->title .= ' | ' . trans('messages.secciones.titulo-general');
        }

        return $this->title;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getTwitterUser() {
        return $this->twitterUser;
    }

    /**
     * @return \SocialLinks\Page
     */
    public function getSocialLinksPage()
    {
        return new Page([
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'image' => $this->getImage(),
            'twitterUser' => $this->getTwitterUser()
        ]);
    }
}
