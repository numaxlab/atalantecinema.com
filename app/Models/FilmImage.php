<?php

namespace App\Models;

use App\Models\Traits\UploadFilesTrait;
use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use ViewHelper;

/**
 * App\Models\FilmImage
 *
 * @property int $id
 * @property string $file_path
 * @property int $film_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Film $film
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FilmImageTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage withTranslation()
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmImage whereRgt($value)
 * @property-read mixed $thumbnail
 */
class FilmImage extends Model
{
    use Translatable, CrudTrait, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'filmes/imaxes';

    public $translatedAttributes = ['name'];

    protected $fillable = [
        'file_path',
        'film_id'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    public function setFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }

    public function getThumbnailAttribute()
    {
        return $this->getThumbnailImgTag();
    }

    public function getThumbnailImgTag($width = 44, $height = 25)
    {
        return sprintf('<img src="%s" alt="%s" />', ViewHelper::imgSrc($this, 'file_path', 'resizeCrop,'.$width.','.$height), '');
    }
}
