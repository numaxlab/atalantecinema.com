<?php

namespace App\Models;

use App\Models\Traits\UploadFilesTrait;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmAward
 *
 * @property int $id
 * @property string $name
 * @property string $logo_file_path
 * @property int $film_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Film $film
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereLogoFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $parent_id
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmAward whereRgt($value)
 */
class FilmAward extends Model
{
    use CrudTrait, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'filmes/premios';

    protected $fillable = [
        'name',
        'logo_file_path',
        'film_id'
    ];

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    public function setLogoFilePathAttribute($value)
    {
        $this->storeFileToDisk($value, 'logo_file_path', self::STORAGE_DISK, self::UPLOADS_DESTINATION_PATH);
    }
}
