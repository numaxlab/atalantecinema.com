<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cinema
 *
 * @property int $id
 * @property string $name
 * @property string $city
 * @property string $website_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cinema whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cinema whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cinema whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cinema whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cinema whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cinema whereWebsiteUrl($value)
 * @mixin \Eloquent
 */
class Cinema extends Model
{
    use CrudTrait;

    protected $fillable = [
        'name',
        'city',
        'website_url'
    ];

    public function filmProjections()
    {
        $this->hasMany(FilmProjection::class);
    }
}
