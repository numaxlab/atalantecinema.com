<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmQuoteTranslation
 *
 * @property int $id
 * @property int $film_quote_id
 * @property string $locale
 * @property string $quote
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuoteTranslation whereFilmQuoteId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuoteTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuoteTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmQuoteTranslation whereQuote($value)
 * @mixin \Eloquent
 */
class FilmQuoteTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['quote'];
}
