<?php

namespace App\Models;

use App\Models\Traits\UploadFilesTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmTranslation
 *
 * @property int $id
 * @property int $film_id
 * @property string $locale
 * @property string $title
 * @property string $slug
 * @property string $original_language
 * @property string $subtitles
 * @property string $origin_countries
 * @property string $synopsis
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereFilmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereOriginCountries($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereOriginalLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereSubtitles($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereSynopsis($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereTitle($value)
 * @mixin \Eloquent
 * @property-read mixed $slug_source
 * @property string $release_date
 * @property string $classification
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereClassification($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmTranslation whereReleaseDate($value)
 */
class FilmTranslation extends Model
{
    use Sluggable, UploadFilesTrait;

    const STORAGE_DISK = 'public';
    const UPLOADS_DESTINATION_PATH = 'filmes';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'slug',
        'original_language',
        'subtitles',
        'origin_countries',
        'synopsis',
        'classification',
        'release_date',
        'trailer',
        'poster_file_path',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_source',
                'unique' => false
            ]
        ];
    }

    public function getSlugSourceAttribute()
    {
        if (! empty(trim($this->slug))) {
            return $this->slug;
        }

        return $this->title;
    }
}
