<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BroadcastingNetwork
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BroadcastingNetwork whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BroadcastingNetwork whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BroadcastingNetwork whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BroadcastingNetwork whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BroadcastingNetwork whereUrl($value)
 * @mixin \Eloquent
 */
class BroadcastingNetwork extends Model
{
    use CrudTrait;

    protected $fillable = [
        'name',
        'url'
    ];

    public function films()
    {
        return $this->belongsToMany(Film::class);
    }
}
