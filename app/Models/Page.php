<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageTranslation[] $translations
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page listsTranslations($translationField)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page translated()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page translatedIn($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page withTranslation()
 * @mixin \Eloquent
 */
class Page extends Model
{
    use Translatable, CrudTrait;

    public $translatedAttributes = ['name', 'slug', 'content'];

    protected $fillable = ['id'];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];
}
