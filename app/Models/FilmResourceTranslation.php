<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmResourceTranslation
 *
 * @property int $id
 * @property int $film_resource_id
 * @property string $locale
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResourceTranslation whereFilmResourceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResourceTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResourceTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmResourceTranslation whereName($value)
 * @mixin \Eloquent
 */
class FilmResourceTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
