<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FilmStreamingMediaTranslation
 *
 * @property int $id
 * @property int $film_streaming_media_id
 * @property string $locale
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMediaTranslation whereFilmStreamingMediaId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMediaTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMediaTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FilmStreamingMediaTranslation whereName($value)
 * @mixin \Eloquent
 */
class FilmStreamingMediaTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
