<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BlogPostTranslation
 *
 * @property int $id
 * @property int $blog_post_id
 * @property string $locale
 * @property string $title
 * @property string $slug
 * @property string $intro
 * @property string $body
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereBlogPostId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereIntro($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BlogPostTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class BlogPostTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;

    protected $fillable = ['title', 'slug', 'intro', 'body'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
