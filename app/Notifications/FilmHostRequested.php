<?php

namespace App\Notifications;

use App\Http\Requests\HostFilmRequest;
use App\Models\Film;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FilmHostRequested extends Notification
{
    use Queueable;

    /**
     * @var Film
     */
    private $film;

    /**
     * @var HostFilmRequest
     */
    private $request;

    /**
     * Create a new notification instance.
     *
     * @param \App\Models\Film $film
     * @param \App\Http\Requests\HostFilmRequest $request
     */
    public function __construct(Film $film, HostFilmRequest $request)
    {
        $this->film = $film;
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $translatedAttributes = __('validation.attributes');
        $formData = [];

        foreach ($this->request->all() as $key => $value) {
            if ($key == '_token' || $key == 'honeypotToken') {
                continue;
            }

            if (array_key_exists($key, $translatedAttributes)) {
                $formData[ucfirst($translatedAttributes[$key])] = $value;
            } else {
                $formData[$key] = $value;
            }
        }

        return (new MailMessage)
            ->subject(__('messages.reserva-proyeccion-titulo-notificacion', ['film_title' => $this->film->title]))
            ->markdown('mail.film.host-request', ['film' => $this->film, 'formData' => $formData]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
