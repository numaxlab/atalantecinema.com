<?php

namespace App\Notifications;

use App\Http\Requests\SubmitFilmRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FilmSubmitted extends Notification
{
    use Queueable;

    /**
     * @var SubmitFilmRequest
     */
    private $request;

    /**
     * Create a new notification instance.
     *
     * @param \App\Http\Requests\SubmitFilmRequest $request
     */
    public function __construct(SubmitFilmRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $translatedAttributes = __('validation.attributes');
        $formData = [];

        foreach ($this->request->all() as $key => $value) {
            if ($key == '_token' || $key == 'honeypotToken') {
                continue;
            }

            if (array_key_exists($key, $translatedAttributes)) {
                $formData[ucfirst($translatedAttributes[$key])] = $value;
            } else {
                $formData[$key] = $value;
            }
        }

        return (new MailMessage)
                    ->subject(__('messages.envia-pelicula-titulo-notification'))
                    ->markdown('mail.film.submitted', ['formData' => $formData]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
