<?php

use Backpack\CRUD\CrudServiceProvider as CRUD;

CRUD::resource('films', 'FilmCrudController');

Route::group(['prefix' => 'films/{filmId}'], function () {
    CRUD::resource('images', 'FilmImageCrudController');
    CRUD::resource('awards', 'FilmAwardCrudController');
    CRUD::resource('quotes', 'FilmQuoteCrudController');
    CRUD::resource('resources', 'FilmResourceCrudController');
    CRUD::resource('projections', 'FilmProjectionCrudController');
    CRUD::resource('streaming-media', 'FilmStreamingMediaCrudController');
});

CRUD::resource('film-makers', 'FilmMakerCrudController');

CRUD::resource('cinemas', 'CinemaCrudController');

CRUD::resource('broadcasting-networks', 'BroadcastingNetworkCrudController');

CRUD::resource('blog-posts', 'BlogPostCrudController');

CRUD::resource('pages', 'PageCrudController');
