<?php

Route::get('/gl/filmes/{slug?}', function ($slug = null) {
    if ($slug) {
        return redirect()->route('film.show', [$slug]);
    }

    return redirect()->route('film.index');
});

Route::get('/es/peliculas/{slug?}', function ($slug = null) {
    if ($slug) {
        return redirect()->route('film.show', [$slug]);
    }

    return redirect()->route('film.index');
});

Route::get('/gl/cineastas/{slug?}', function ($slug = null) {
    if ($slug) {
        return redirect()->route('film-maker.show', [$slug]);
    }

    return redirect()->route('film-maker.index');
});

Route::get('es/cineastas/{slug?}', function ($slug = null) {
    if ($slug) {
        return redirect()->route('film-maker.show', [$slug]);
    }

    return redirect()->route('film-maker.index');
});

Route::get('gl/caderno/{slug?}', function ($slug = null) {
    if ($slug) {
        return redirect()->route('blog-post.show', [$slug]);
    }

    return redirect()->route('blog-post.index');
});

Route::get('es/cuaderno/{slug?}', function ($slug = null) {
    if ($slug) {
        return redirect()->route('blog-post.show', [$slug]);
    }

    return redirect()->route('blog-post.index');
});

Route::get('gl/quen-somos', function () {
    return redirect()->route('about');
});

Route::get('es/quiene-somos', function () {
    return redirect()->route('about');
});

Route::get('gl/na-casa', function () {
    return redirect()->route('vod.index');
});

Route::get('es/en-casa', function () {
    return redirect()->route('vod.index');
});
